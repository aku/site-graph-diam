# Get a graph diameter build on top of a website links

Here are two scripts:
* collect_links.py
* graph_diameter.py

The first one recursively collects links from a given URL and writes collected
links to a file.
The second one reads the resulting file builds an unweighted directed graph
based on the collected links and calculates it's diameter.
Both scripts are tested with python3.5

## Installation

* Clone the repo
* cd to the repo
* create virtualenv: `virtualenv venv`
* activate created venv: `. venv/bin/activate`
* install requirements: `pip install -r requirements.txt`

## Collecting links

To collect links run

```
python collect_links.py --url http://some_site.com/target/page -N 2 \
-o some_site.results
```

To get all script options type `python collect_links.py -h`

## Get graph diameter

To get a graph diameter from the collected links run:

```
python graph_diameter.py -i some_site.results
```

To get all script options type `python graph_diameter.py -h`

## Tests

There are a couple of tests to test BFS travesal algorithm which is used under the
hood in graph_diameter.py script. To run it:

```
python test_graph_diameter.py
```
