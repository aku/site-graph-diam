import unittest

import graph_diameter


class TestBFSNodeA(unittest.TestCase):
    graph = {
        'a': ['b', 'c', 'd', 'f3'],
        'b': ['b1', 'b2'],
        'b1': ['a', 'c'],
        'b2': ['a', 'd', 'f'],
        'f': ['f1'],
        'f1': ['f2'],
        'f2': ['f3'],
    }
    node = 'a'
    expected_path = ('a', 'b', 'b2', 'f', 'f1', 'f2')
    expected_diam = len(expected_path) - 1

    def runTest(self):
        diam, path = graph_diameter.bfs_traversal_node(self.graph, self.node)
        self.assertEqual(self.expected_path, path)
        self.assertEqual(self.expected_diam, diam)

        diam, endpoints = graph_diameter.bfs_traversal_node_diam_only(
            self.graph, self.node
        )
        self.assertEqual(
            (self.expected_path[0], self.expected_path[-1]),
            endpoints
        )
        self.assertEqual(self.expected_diam, diam)


class TestBFSNodeB(TestBFSNodeA):
    node = 'b'
    expected_path = ('b', 'b2', 'f', 'f1', 'f2')
    expected_diam = len(expected_path) - 1


class TestBFSNodeB1(TestBFSNodeA):
    node = 'b1'
    expected_path = ('b1', 'a', 'b', 'b2', 'f', 'f1', 'f2')
    expected_diam = len(expected_path) - 1


class TestBFSNodeB2(TestBFSNodeA):
    node = 'b2'
    expected_path = ('b2', 'a', 'b', 'b1')
    expected_diam = len(expected_path) - 1


class TestBFSNodeF(TestBFSNodeA):
    node = 'f'
    expected_path = ('f', 'f1', 'f2', 'f3')
    expected_diam = len(expected_path) - 1


if __name__ == "__main__":
    unittest.main()
