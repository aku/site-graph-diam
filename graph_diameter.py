#!/bin/env python3
"""The script computes a diameter (the length of the longest shortest path)
for a directed graph.
A graph should be stored in a file. Each line of a file should be
a json-formatted string:
    {key: value}
Where 'key' - name of the graph's node (a string or a number)
'value' - list of another nodes, which are directly accessible from the 'key'
The file could also contain empty strings and comments
(strings started with '#') - both will be ignored.

It uses Breadth-first search algorithm to find an eccentricity for each
node which has children connected.
Time complexity estimation for the algorithm is O(|V| * (|V| + |E|))
where V - number of vertices in the graph, E - number of edges
"""
import argparse
from collections import deque, OrderedDict
import json
import logging
import sys
import time

# Skip lines in an input file if it starts with this symbol
COMMENT_SYM = '#'


LOG = logging.getLogger(__name__)
LOG_LEVELS = OrderedDict((
    ('CRITICAL', logging.CRITICAL),
    ('ERROR', logging.ERROR),
    ('WARNING', logging.WARNING),
    ('INFO', logging.INFO),
    ('DEBUG', logging.DEBUG),
))


def setup_logs(log_level=logging.DEBUG):
    """Setups logging with the given log level, logs will be written to
    stderr
    """
    LOG.setLevel(log_level)
    handler = logging.StreamHandler(sys.stderr)
    LOG.addHandler(handler)


def get_args():
    """Defines and reads cli parameters for the script."""
    parser = argparse.ArgumentParser(
        description=(
            'Compute graph diameter for a directed graph recorded in a file.\n'
            'Each line of the file should be a json like:\n'
            '{"Name": [list of vertices connected to the Name]}'
        )
    )

    parser.add_argument(
        '-i', '--input-file',
        dest='input_file',
        type=argparse.FileType('r'),
        required=True
    )

    parser.add_argument(
        '--get-metrics',
        dest='get_metrics',
        action='store_true',
        help='Just get the basic graph metrics',
    )

    parser.add_argument(
        '--log-level',
        default='INFO',
        choices=LOG_LEVELS.keys(),
    )

    parser.add_argument(
        '--get-path',
        action='store_true',
        help=(
            'Also get the complete longest path. '
            'It will slow down the computation (approx. 2-3 times). '
            'Without the option the result will contain only endpoints for a '
            'found longest path'
        )
    )

    return parser.parse_args()


def load_graph(infile):
    """Loads graph from a given input file (or file-like) object.
    Returns a dict with graph, where keys are node names, values are lists
    of connected nodes.
    """
    result = {}
    LOG.info("Loading graph")
    for line in infile:
        line = line.strip()
        if not line or line.startswith(COMMENT_SYM):
            continue
        data = json.loads(line)
        result.update(data)
    LOG.info(
        "Graph has been loaded. Total number of vertices with children: %s",
        len(result)
    )
    return result


def compute_diameter(infile, with_path=False):
    """Loads graph from the infile, and runs diameter computation.
    :param infile: file object to read
    :param with_path: boolean flag. If set, then will be computed and returned
        the longest path alongside with found graph's diameter.
    :return: tuple of diameter found, and according longest path
        if with_path flag is not set then will be returned an empty list
        instead of the longest path.
    """
    graph = load_graph(infile)
    all_vertices, edgenum = _get_metrics(graph)
    LOG.info(
        "Total number of vertices: %s. Edges: %s", len(all_vertices), edgenum
    )
    LOG.info("Starting traversal")
    diam, longest_path = bfs_traversal(graph, with_path)
    LOG.info(
        "Traversal is complete. Diam: %s, longest path: %s",
        diam, longest_path
    )
    return diam, longest_path


def bfs_traversal(graph, with_path):
    """Computational loop.
    Runs BFS traversal for every node (which is a key in `graph`) from
    the given graph.
    """
    longest = None
    diam = 0
    counter = 0
    total_count = len(graph)
    start_time = time.time()
    for start in graph:
        counter += 1
        LOG.info("NEW START POINT (%s of %s): %s", counter, total_count, start)
        if with_path:
            newlength, newlongest = bfs_traversal_node(graph, start)
        else:
            newlength, newlongest = bfs_traversal_node_diam_only(graph, start)
        LOG.debug(
            "Exploring of %s is complete. "
            "Longest path length for the node = %s",
            start, newlength
        )
        if newlength > diam:
            longest = newlongest
            diam = newlength
        LOG.info("DIAM: %s. The longest path: %s", diam, longest)

        total_time = time.time() - start_time
        time_per_node = total_time / float(counter)
        remained_time_estimation = time_per_node * (total_count - counter)
        LOG.info("Total time: %s for %s nodes", total_time, counter - 1)
        LOG.info("Estimated remained time: %s", remained_time_estimation)

    return diam, longest


def bfs_traversal_node(graph, start):
    """Breadth-first search algorithm. Stores traversed longest paths.
    """
    queue = deque([(start, (start,))])
    longest = [start]
    visited = set()
    while queue:
        (node, path) = queue.popleft()
        if node in visited:
            continue
        visited.add(node)
        for next_ in graph[node]:
            if next_ in visited:
                if not longest or len(path) > len(longest):
                    longest = path
                continue
            new_path = path + (next_,)
            if not longest or len(new_path) > len(longest):
                longest = new_path
            if next_ not in graph:
                visited.add(next_)
                continue
            queue.append((next_, new_path))
    return len(longest) - 1, longest


def bfs_traversal_node_diam_only(graph, start):
    """BFS algorithm. Stores only maximum distance found, without paths.
    """
    queue = deque([(start, 0)])
    longest_length = 0
    last_endpoint = None
    visited = set()
    while queue:
        node, length = queue.popleft()
        if node in visited:
            continue
        visited.add(node)
        for next_ in graph[node]:
            new_length = length + 1
            if next_ in visited:
                continue
            if new_length > longest_length:
                longest_length = new_length
                last_endpoint = next_
            if next_ not in graph:
                visited.add(next_)
                continue
            queue.append((next_, new_length))
    return longest_length, (start, last_endpoint)


def compute_graph_metrics(input_file):
    """Loads graph for the given file and returns vertices number and edges
    number.
    """
    graph_dict = load_graph(input_file)
    return _get_metrics(graph_dict)


def _get_metrics(graph):
    all_vertices = set()
    edge_num = 0
    for node, edges in graph.items():
        all_vertices |= set(edges)
        all_vertices.add(node)
        edge_num += len(edges)
    return all_vertices, edge_num


def run():
    """Reads cli params, runs computations and prints results.
    """
    args = get_args()
    setup_logs(args.log_level)
    if args.get_metrics:
        vertices, edgenum = compute_graph_metrics(args.input_file)
        print("Vertices: {}, Edges: {}".format(len(vertices), edgenum))
    else:
        diameter, longestpath = compute_diameter(
            args.input_file, args.get_path
        )
        print("Diameter:", diameter)
        if longestpath is not None:
            print("Path:", longestpath)


if __name__ == '__main__':
    run()
