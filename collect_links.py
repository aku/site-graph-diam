#!/bin/env python3
"""The script recursively collects links from a given URL and writes collected
links line by line, where each line is a json-formatted string
{key: value}
key - explored URL, value - list of links found at the page by the URL.
It accepts the following parameters:
* depth to restrict exploration depth
* stop-url - a list of urls which should not be explored, e.g. main page for
  a wikipedia article

It uses scrapy lib under the hood.

Note: it collects links only for the domain specified in starting URL.

"""
import argparse
from collections import OrderedDict
import json
import logging
import sys
from urllib.parse import urlparse

import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.crawler import CrawlerProcess


LOG = logging.getLogger(__name__)
LOG_LEVELS = OrderedDict((
    ('CRITICAL', logging.CRITICAL),
    ('ERROR', logging.ERROR),
    ('WARNING', logging.WARNING),
    ('INFO', logging.INFO),
    ('DEBUG', logging.DEBUG),
))


def setup_logs(log_level=logging.DEBUG):
    """Sets current log level and put the log to stderr"""
    LOG.setLevel(log_level)
    handler = logging.StreamHandler(sys.stderr)
    LOG.addHandler(handler)


def setup_scrapy_logs():
    """Sets log parameters for scrapy. Makes it the same as the script log
    parameters.
    """
    scrapy_logger = logging.getLogger('scrapy')
    scrapy_logger.setLevel(LOG.getEffectiveLevel())
    scrapy_logger.handlers = LOG.handlers


def get_args():
    """Parses cli args, returns parsed arguments.
    """
    parser = argparse.ArgumentParser(
        description=(
            "Collect links from a url for a given depth"
        )
    )

    parser.add_argument(
        '-N', '--depth',
        dest='depth',
        default=2,
        help='Specify a depth',
        type=int,
    )

    parser.add_argument(
        '--log-level',
        default='DEBUG',
        choices=LOG_LEVELS.keys(),
    )

    parser.add_argument(
        '--stop-urls',
        dest='stop_urls',
        default=[],
        nargs='+',
        help=(
            "List of urls which shouldn't be explored.\n"
            "E.g. for a wikipedeia article you may don't want to follow "
            "links from main page, so put it here: \n"
            "--stop-urls https://en.wikipedia.org/wiki/Main_Page"
        )
    )

    parser.add_argument(
        '-o', '--outfile',
        dest='outfile',
        type=argparse.FileType('w'),
        default=sys.stdout,
        help='Output file or stdout',
    )

    parser.add_argument(
        '--url',
        required=True,
        help='Start URL to explore'
    )

    return parser.parse_args()


def run_crawl(maxdepth, url, outfile, stop_urls=None):
    """Configures spider class and runs scrapy crawler with it.
    """
    LOG.debug('Starting crawling of "%s" for depth %s', url, maxdepth)
    process = CrawlerProcess({
        'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)'
    })
    LinksSpider.init_spider(url, maxdepth, outfile, stop_urls or [])
    process.crawl(LinksSpider)
    setup_scrapy_logs()
    process.start()


def get_domain(url):
    """Returns domain name extracted from the given URL"""
    parse_result = urlparse(url)
    return parse_result.netloc


class LinksSpider(scrapy.Spider):
    """Spider class for recursive links collecting.
    Before use it, call init_spider method to setup crawling parameters.
    """
    name = 'LinksSpider'
    allowed_domains = []
    start_urls = []
    custom_settings = {}
    visited = set()
    outfile = None
    maxdepth = 1

    @classmethod
    def init_spider(cls, url, maxdepth, outfile, stop_urls):
        """
        :param url: staring URL to explore
        :param maxdepth: maximum depth to crawl from starting URL
        :param outfile: file object where will be stored crawling results
        :param stop_urls: list or urls which will not be explored
        """
        cls.allowed_domains = [get_domain(url)]
        cls.start_urls = [url]
        cls.custom_settings = {
            'DEPTH_LIMIT': maxdepth,
        }
        cls.outfile = outfile
        cls.stop_urls = stop_urls
        cls.visited = set()
        cls.maxdepth = maxdepth

    def parse(self, response):
        """Extracts links from the response.
        Extracted links are saved in self.outfile in form:
            {"current URL": [list of extracted links]}\n
        If the max depth is not reached
        """
        url = response.url
        self.visited.add(url)
        curr_depth = response.meta.get('depth', 1)
        links_list = [
            item.url for item in
            LinkExtractor(
                allow_domains=self.allowed_domains,
                unique=True,
                canonicalize=True,
            ).extract_links(response)
            # We want links to another pages, not to self
            if item.url != url
        ]
        self.outfile.write(json.dumps({url: links_list}) + '\n')
        if curr_depth + 1 > self.maxdepth:
            return
        for alink in links_list:
            if alink in self.visited or alink in self.stop_urls:
                continue
            yield scrapy.http.Request(alink, meta={'depth': curr_depth + 1})


def run():
    """Reads cli args and runs crawling"""
    args = get_args()
    setup_logs(LOG_LEVELS[args.log_level])
    run_crawl(args.depth, args.url, args.outfile, args.stop_urls)


if __name__ == '__main__':
    run()
